@extends('layouts.master')
@section('title','List project')
@section('content')

<div class="container">
    <h1 class="text-center my-2">List project GitLab</h1>
  <!-- Button trigger modal -->
  <button type="button" class="btn btn-primary my-2" data-toggle="modal" data-target="#exampleModal">
    Tambah project repo Baru
  </button>

  <table class="table table-striped">
                <thead>
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Namespace</th>
                    <th scope="col">URL</th>
                    <th scope="col">Star</th>
                    <th scope="col">created at</th>
                  </tr>
                </thead>
                <tbody>
                <?php $i=1;?>
                    @foreach ($plStandings as $item)
                  <tr>
                    <th scope="row">{{ $i}}</th>
                    <td>{{ $item->name_with_namespace}}</td>
                    <td>{{ $item->web_url}}</td>
                    
                    <td><i class="far fa-star"></i> {{ $item->star_count}}</td>
                    <td>{{ $item->created_at}}</td>
                </tr>    
                {{$i++  }}            
                @endforeach 
                </tbody>
              </table>  
       
</div>
{{-- <h4>{{ $listProject[0]->description}}</h4> --}}
    


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="" method="post">
          {{-- <label for="nameRep">Nama Repository Baru</label> --}}
          <input type="text" name="nameRep" id="nameRep" placeholder="Nama Repository Baru">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Add</button>
      </div>
    </div>
  </div>
</div>

@endsection

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class project_git extends Controller
{
    public function index(){
        $client = new Client();
        $uri = 'https://gitlab.com/api/v4/users/016apih/projects';
        $header = array('headers' => array('PRIVATE_Token' => 'h1idT_sJXobQdA4y3626'));
        $response = $client->get($uri, $header);

        $plStandings = json_decode($response->getBody()->getContents());
        return view('project_git.listproject', compact('plStandings'));
    
    }
    }